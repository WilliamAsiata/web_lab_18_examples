DROP TABLE IF EXISTS socket_registered_users;

CREATE TABLE socket_registered_users (	
	firstname varchar(32),
	lastname varchar(32),
	username varchar(32) not null,
	email varchar(50),
	primary key(username),
	check(email like '%@%.___')
);

insert into socket_registered_users values('me', 'meme', 'Me1', 'me@here.com');
insert into socket_registered_users values('pinky', NULL, 'Pinky', 'pinky@here.com');
insert into socket_registered_users values('toto', NULL, 'Toto', 'momo@there.com');
insert into socket_registered_users values('Pete', 'Dodo', 'Pete', 'pete@here.com');
insert into socket_registered_users values('Mumu', 'Peterson', 'Mumu', 'peterson@here.com');
insert into socket_registered_users values('nono', 'momo', 'Nono', 'nono@there.com');
insert into socket_registered_users values('Peter', 'Lala', 'Peter', 'peterlala@here.com');