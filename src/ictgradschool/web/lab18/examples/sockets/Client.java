package ictgradschool.web.lab18.examples.sockets;

import ictgradschool.Keyboard;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * A simple TCP Client {@link java.net.Socket} app which allows the user to send two ints to a server for addition.
 */
public class Client {

    public static final String HOST = "localhost";
    public static final int PORT = 8181;

    private void start() {

        boolean loop = true;

        while(loop) {

            System.out.print("Enter the first number: > ");
            int num1 = Integer.parseInt(Keyboard.readInput());

            System.out.print("Enter the second number: > ");
            int num2 = Integer.parseInt(Keyboard.readInput());

            try (Socket client = new Socket(HOST, PORT)) {

                DataOutputStream out = new DataOutputStream(client.getOutputStream());
                out.writeInt(num1);
                out.writeInt(num2);
                System.out.println("Sent numbers to server.");

                DataInputStream in = new DataInputStream(client.getInputStream());
                int result = in.readInt();
                System.out.println("Read result from server: " + num1 + " + " + num2 + " = " + result);

                System.out.print("Perform another calculation (y/n)? > ");
                String s = Keyboard.readInput();
                loop = s.toLowerCase().startsWith("y");

            } catch (IOException e) {

                System.out.println("Error: " + e.getMessage());
                e.printStackTrace();
                loop = false;

            }

        }
    }

    public static void main(String[] args) {
        new Client().start();
    }
}
